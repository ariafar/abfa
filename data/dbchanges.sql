ALTER TABLE  `user` ADD  `region_id` INT NULL;
ALTER TABLE `user` ADD FOREIGN KEY (region_id) REFERENCES regions(id);

ALTER TABLE  `user` ADD  `photo` VARCHAR( 255 ) NULL;

ALTER TABLE  `events` CHANGE  `user_id`  `creator_id` INT( 11 ) NULL DEFAULT NULL

CREATE TABLE IF NOT EXISTS `event_users` (
      `event_id` varchar(50) COLLATE utf8_persian_ci NOT NULL,
      `user_id` int(11) NOT NULL,
      PRIMARY KEY (`event_id`,`user_id`),
      KEY `user_id` (`user_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
ALTER TABLE `event_users` ADD FOREIGN KEY (event_id) REFERENCES events(id);
ALTER TABLE `event_users` ADD FOREIGN KEY (user_id) REFERENCES user(id);



ALTER TABLE  `events` ADD  `update_time` DATETIME NULL;

ALTER TABLE  `resources` ADD  `unit` VARCHAR( 64 ) NULL;
ALTER TABLE  `user` ADD  `position` VARCHAR( 64 ) NULL;





---------------------------
ALTER TABLE  `user` ADD  `type` ENUM(  'ADMIN',  'USER' ) NOT NULL DEFAULT  'USER';
ALTER TABLE  `user` ADD  `firstname` VARCHAR( 50 ) NULL , ADD  `lastname` VARCHAR( 50 ) NULL



---------------------------
ALTER TABLE `events` ADD `readonly` BOOLEAN NOT NULL DEFAULT FALSE ;
ALTER TABLE `events` ADD `source` ENUM('MOBILE','WEBSERVICE') NOT NULL DEFAULT 'MOBILE' ;



----------------
ALTER TABLE `user` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `causes` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `type_categories` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `types` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `levels` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `regions` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `resource_categories` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `resources` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `coordinations` ADD `modified_date` DATETIME NOT NULL DEFAULT '2012-06-23 00:00:00' ;
ALTER TABLE `user` ADD `accessibility` BOOLEAN NOT NULL DEFAULT FALSE ;
CREATE TABLE IF NOT EXISTS `user_122accounts` (
	`id` int(11) NOT NULL,
	  `user_id` int(11) NOT NULL,
	  `username` varchar(255) COLLATE utf8_persian_ci NOT NULL,
	  `password` varchar(255) COLLATE utf8_persian_ci NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

ALTER TABLE `user_122accounts` ADD PRIMARY KEY (`id`);
ALTER TABLE `user_122accounts` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contacts` (
    `id` int(11) NOT NULL,
      `group_id` int(11) NOT NULL,
      `title` varchar(255) COLLATE utf8_persian_ci DEFAULT NULL,
      `firstname` varchar(255) COLLATE utf8_persian_ci DEFAULT NULL,
      `lastname` varchar(255) COLLATE utf8_persian_ci DEFAULT NULL,
      `cell_phone` varchar(20) COLLATE utf8_persian_ci DEFAULT NULL,
      `home_phone` varchar(20) COLLATE utf8_persian_ci DEFAULT NULL
    ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

CREATE TABLE IF NOT EXISTS `contact_groups` (
    `id` int(11) NOT NULL,
      `title` varchar(255) COLLATE utf8_persian_ci NOT NULL
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

CREATE TABLE IF NOT EXISTS `contact_group_users` (
      `user_id` int(11) NOT NULL,
      `contact_group_id` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

ALTER TABLE `contacts`  ADD PRIMARY KEY (`id`), ADD KEY `contact_groups` (`group_id`);
ALTER TABLE `contact_groups`  ADD PRIMARY KEY (`id`);

ALTER TABLE `contact_group_users`  ADD PRIMARY KEY (`user_id`,`contact_group_id`), ADD KEY `contact_group_id` (`contact_group_id`);

ALTER TABLE `contacts` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
ALTER TABLE `contact_groups` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `contacts` ADD CONSTRAINT `contact_groups` FOREIGN KEY (`group_id`) REFERENCES `contact_groups` (`id`);

ALTER TABLE `contact_group_users`
ADD CONSTRAINT `contact_group_users_ibfk_2` FOREIGN KEY (`contact_group_id`) REFERENCES `contact_groups` (`id`),
ADD CONSTRAINT `contact_group_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);



CREATE TABLE IF NOT EXISTS `shifts` (
    `id` int(11) NOT NULL,
      `date` date NOT NULL,
      `user_id` int(11) NOT NULL,
      `creation_date` datetime NOT NULL
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

    --
ALTER TABLE `shifts` ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);
ALTER TABLE `shifts` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `shifts` ADD CONSTRAINT `shifts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);






ALTER TABLE `contacts` ADD `image` VARCHAR(255) NULL ;
ALTER TABLE `External_News` ADD `status` ENUM('published', 'not_processed', 'unpublished') NOT NULL DEFAULT 'not_processed' ;




ALTER TABLE `shifts` ADD `update_date` DATETIME NULL ;
