(function( $ ){
	$.fn.serializeJSON=function() {
		var json = {};
		jQuery.map($(this).serializeArray(), function(n, i){
		json[n['name']] = n['value'];
		});
		return json;
};
})( jQuery );




(function( $ ){
	$.fn.validateDate=function(value) {
	    var check = false;
            var re = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
            if( re.test(value)){
                var adata = value.split('-');
                var dd = parseInt(adata[2],10); // was gg (giorno / day)
                var mm = parseInt(adata[1],10); // was mm (mese / month)
                var yyyy = parseInt(adata[0],10); // was aaaa (anno / year)
                var xdata = new Date(yyyy,mm-1,dd);
                if ( ( xdata.getFullYear() == yyyy ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == dd ) )
                    check = true;
                else
                    check = false;
            } else
                check = false;
            return check;
};
})( jQuery );

(function($, undefined) {
    $.fn.getCursorPosition = function(input) {
        if ("selectionStart" in input && document.activeElement == input) {
            return {
                start: input.selectionStart,
                end: input.selectionEnd
            };
        }
        else if (input.createTextRange) {
            var sel = document.selection.createRange();
            if (sel.parentElement() === input) {
                var rng = input.createTextRange();
                rng.moveToBookmark(sel.getBookmark());
                for (var len = 0;
                        rng.compareEndPoints("EndToStart", rng) > 0;
                        rng.moveEnd("character", -1)) {
                    len++;
                }
                rng.setEndPoint("StartToStart", input.createTextRange());
                for (var pos = {start: 0, end: len};
                        rng.compareEndPoints("EndToStart", rng) > 0;
                        rng.moveEnd("character", -1)) {
                    pos.start++;
                    pos.end++;
                }
                return pos;
            }
        }
        return -1;
    }
})(jQuery);

(function( $ ) {    
    $.fn.setToFixedWith = function(n) {        
        var width = $(this).width(),
                html = '<span style="white-space:nowrap">',
                line = $(this).wrapInner(html).children()[ 0 ];                

        $(this).css('font-size', n);

        while ($(line).width() > width) {
            $(this).css('font-size', --n);
        }

    };
})(jQuery);


(function( $ ){
   $.fn.rotate = function(until, step, initial, elt) {
    var _until = until;
    var _step = (!step)?1:step;
    var _initial = initial;
    var _elt = (!elt)?$(this):elt;

    var deg = _initial + _step;

    var browser_prefixes = ['-webkit', '-moz', '-o', '-ms'];
    for (var i=0, l=browser_prefixes.length; i<l; i++) {
      var pfx = browser_prefixes[i]; 
      _elt.css(pfx+'-transform', 'rotate('+deg+'deg)');
    }

    if ((step > 0 && deg < _until) ||  (step < 0 && deg > _until)) {
      setTimeout(function() {
          $(this).rotate(_until, _step, deg, _elt); //recursive call
      }, 5);
    }
};
})(jQuery);


(function( $ ){
jQuery.fn.ResizeImg = function (iH, iW, W, H) {
    var oH = H ? H : $(window).height();
    var oW = W ? W : $(window).width();   
    var iH = iH ? iH : oH;
    var iW = iW ? iW : 800;   
    
    var opt = {
        width : iW,
        height : iH         
    };

    // When the image is too small so, do nothing
    if(!(iW < oW && iH < oH)){
        // Otherwise, proportionate the image relative 
        // to its container
        if(oH / iH > oW / iW){
            opt.width = oW;
            opt.height = iH*(oW/iW);
        } else {
            opt.width = iW*(oH/iH);
            opt.height = oH;            
        }
    }
    opt.width = Math.ceil(opt.width);
    opt.height = Math.ceil(opt.height);
    return opt;
};
})(jQuery);

/*	

	jQuery pub/sub plugin by Peter Higgins (dante@dojotoolkit.org)

	Loosely based on Dojo publish/subscribe API, limited in scope. Rewritten blindly.

	Original is (c) Dojo Foundation 2004-2009. Released under either AFL or new BSD, see:
	http://dojofoundation.org/license for more information.

*/	

(function(d){

	// the topic/subscription hash
	var cache = {};

	d.publish = function(/* String */topic, /* Array? */args){
		// summary: 
		//		Publish some data on a named topic.
		// topic: String
		//		The channel to publish on
		// args: Array?
		//		The data to publish. Each array item is converted into an ordered
		//		arguments on the subscribed functions. 
		//
		// example:
		//		Publish stuff on '/some/topic'. Anything subscribed will be called
		//		with a function signature like: function(a,b,c){ ... }
		//
		//	|		$.publish("/some/topic", ["a","b","c"]);
		d.each(cache[topic], function(){
			this.apply(d, args || []);
		});
	};

	d.subscribe = function(/* String */topic, /* Function */callback){
		// summary:
		//		Register a callback on a named topic.
		// topic: String
		//		The channel to subscribe to
		// callback: Function
		//		The handler event. Anytime something is $.publish'ed on a 
		//		subscribed channel, the callback will be called with the
		//		published array as ordered arguments.
		//
		// returns: Array
		//		A handle which can be used to unsubscribe this particular subscription.
		//	
		// example:
		//	|	$.subscribe("/some/topic", function(a, b, c){ /* handle data */ });
		//
		if(!cache[topic]){
			cache[topic] = [];
		}
		cache[topic].push(callback);
		return [topic, callback]; // Array
	};

	d.unsubscribe = function(/* Array */handle){
		// summary:
		//		Disconnect a subscribed function for a topic.
		// handle: Array
		//		The return value from a $.subscribe call.
		// example:
		//	|	var handle = $.subscribe("/something", function(){});
		//	|	$.unsubscribe(handle);
		
		var t = handle[0];
		cache[t] && d.each(cache[t], function(idx){
			if(this == handle[1]){
				cache[t].splice(idx, 1);
			}
		});
	};

})(jQuery);



var g_days=[31,28,31,30,31,30,31,31,30,31,30,31], j_days=[31,31,31,31,31,31,30,30,30,30,30,29];
function gregorianToJalali(g_y, g_m, g_d)
{
    g_y = parseInt(g_y);
    g_m = parseInt(g_m);
    g_d = parseInt(g_d);
    var gy = g_y-1600;
    var gm = g_m-1;
    var gd = g_d-1;
    var g_day_no = 365*gy+parseInt((gy+3) / 4)-parseInt((gy+99)/100)+parseInt((gy+399)/400);
    for (var i=0; i < gm; ++i)
        g_day_no += g_days[i];
    if (gm>1 && ((gy%4==0 && gy%100!=0) || (gy%400==0)))
        ++g_day_no;
    g_day_no += gd;
    var j_day_no = g_day_no-79;
    var j_np = parseInt(j_day_no/ 12053);
    j_day_no %= 12053;
    var jy = 979+33*j_np+4*parseInt(j_day_no/1461);
    j_day_no %= 1461;
    if(j_day_no >= 366)
    {
        jy += parseInt((j_day_no-1)/ 365);
        j_day_no = (j_day_no-1)%365;
    }
    for(var i = 0; i < 11 && j_day_no >= j_days[i]; ++i)
        j_day_no -= j_days[i];
    var jm = i+1;
    var jd = j_day_no+1;
    return [jy,jm,jd];
}

(function($) {
    $.fn.showText = function(content) {        
        $("#news-list-toolbar .hide-text-news").css('right', '0');
        
        $("#news-list-toolbar .hide-text-news").animate({
            right:'+=' + $('#news-list-toolbar').width() + 'px'
        },5000,'swing');    
    };

})(jQuery);