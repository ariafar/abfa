define([
    'jquery',
    'underscore',
    'views/list',
    'text!views/templates/external_news_list.html',
    'text!views/templates/external_news_item.html',
    'collections/external-news',
    'i18n!nls/fa-ir/labels',
], function($, _, ListView, ListTpl, ItemTpl, Collection, Labels) {

    var ExternalNewsListView = ListView.extend({

        initialize: function(options) {

            this.events = _.extend({
                "click .publish-action"    : 'publishAction'
            }, this.events);

            this.collection.type = "published";
            ExternalNewsListView.__super__.initialize.call(this, this.options);
        },
        render: function(fields) {
            var tpl = _.template(ListTpl);
            this.$el.html(tpl({
                labels: Labels
            }));
            return this;
        },

        changeTab: function(e) {
            var el = $(e.target),
                type = el.data("type");

            if (el.hasClass("selected"))
                return;

            $(".second-navbar .selected", this.$el).removeClass("selected");
            el.addClass("selected");

            this.collection.type = type;
            this.collection.reset();
            this.collection.fetch();
        },

        reset : function(){
            $(".panel-group", this.$el).empty();
        },

        renderItem  : function(model){
            var tpl = _.template(ItemTpl);
            $(".panel-group", this.$el).append(tpl(_.extend(model.toJSON(), {
                type        : this.collection.type || "published",
                labels      : Labels
            })));
        },

        publishAction : function(e){
            var target = $(e.target),
                el = target.closest(".news-item"),
                id = el.attr("id"),
                model = this.collection.get(id),
                action = target.data("action"),
                $this = this;

            if(model){
                model.publishAction(action, function(){
                    $this.collection.remove(model);
                    el.remove();
                })
            }


        }

    });
    return ExternalNewsListView
});
