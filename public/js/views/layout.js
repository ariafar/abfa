define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'text!views/templates/toolbar.html',
    'text!views/templates/layout.html',
    'views/list',
    'views/calendar',
    'views/contact-groups',
    'views/external-news',
    'i18n!nls/labels',
], function($, Backbone, _, Application, ToolbarTpl, LayoutTpl, ListView, CalendarView, ContactGroupsView, ExternalNewsView, Labels) {

    var layoutView = Backbone.View.extend({
        className: 'row',
        events: {
            'click .right-navigation-panel li'      : 'changeContent',
            'click #changePassword'                 : 'changePassword'
        },
        initialize: function() {
            var toolbarTpl = _.template(ToolbarTpl);

            $('header').html(toolbarTpl({
                labels: Labels
            }));
        },
        render: function() {
            var layoutTpl = _.template(LayoutTpl);

            this.$el.html(layoutTpl({
                labels: Labels
            }));

            $("#main-container-wrapper").html(this.$el);

            return this;
        },
        afterRender: function() {
            $(".right-navigation-panel li:first").trigger("click");
        },
        changeContent: function(e) {
            var el = $(e.target).closest("li");
            this.type = el.data("index");

            if (el.hasClass("selected"))
                return;

            $(".right-navigation-panel .selected").removeClass("selected");
            el.addClass("selected");
            this.getItems()
        },
        getItems: function() {

            var $this = this,
                listView;

            if (this.type == "shifts") {
                this.showCalendar();
                return;

            }
            
            switch (this.type) {
                case "contact-groups":
                    listView = ContactGroupsView;
                    break;

                case "external-news":
                    listView = ExternalNewsView;
                    break;

                default :
                    listView = ListView;
                    break;
            }

            this.listView && this.listView.dispose();

            require(['collections/' + this.type, 'models/' + ((this.type == "news") ? "news" : this.type.slice(0, -1))], function(Collection, Model) {
                $this.listView = new listView({
                    collection: ($this.type == "users") ? window.usersCollection : new Collection([]),
                    type: $this.type,
                    itemModel: Model
                });

                $(".page-content", $this.$el).html($this.listView.$el);
            });
        },
        changePassword: function() {
            if (!window.currentUser.get('id')) {
                Backbone.history.navigate('', true);
                return;
            }
            else {
                require(['views/edit_profile'], function(EditProfileView) {
                    var editProfile = new EditProfileView({
                        type: 'changePassword'
                    });
                    $("#main-container-wrapper").html(editProfile.$el);
                    editProfile.registerEvents();
                });
            }
        },
        showCalendar: function() {
            var calendar = new CalendarView();
            $(".page-content", this.$el).html(calendar.$el);
            calendar.render();
        }

    });

    return layoutView;
});
