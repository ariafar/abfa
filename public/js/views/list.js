define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/list_view',
    'collections/categories',
    'models/category',
    'text!views/templates/list.html',
    'views/item',
    'views/user_form',
    'views/news_form',
    'views/keyword_form',
    'i18n!nls/fa-ir/labels',
], function($, _, Backbone, BaseView, CategoriesCollection, CategoryModel, ListTpl, ItemView, UserForm, NewsForm, KeywordForm, Labels) {

    var ItemsListView = BaseView.extend({
        className: "items-wrapper",
        hasMoreItems: true,
        events: {
            'click .add-item'                   : 'showAddPopup',
            'click .next-page'                  : 'nextPage',
            'click .create-item'                : 'createNewItem',
            'click .update-item'                : 'updateItem',
            'click .second-navbar span'         : 'changeTab',
            'change .category-selection'        : 'setCategory',
            'click .itme-wrapper .btn-default'  : 'showEditPopup',
            'keyup .search'                     : 'search'
        },
        initialize: function(options) {
            this.template = _.template(ListTpl);
            this.options = options;
            var $this = this;
            this.items = [];

            _.bindAll(this, 'onScroll');

            this.ItemModel = options.itemModel;
            this.render();

            if (this.options.filter)
                this.collection.filters.set(this.options.filter, {
                    silent: true
                });

            //if has category fetch categories collection
            if (this.options.type == "resources" || this.options.type == "types") {
                this.categoriesCollection = new CategoriesCollection([]);
                this.categoriesCollection.type = this.options.type;
                this.categoriesCollection.fetch({
                    async: false,
                    success: function() {
                        console.log("Categories Fetched")
                    }
                });

                this.categoriesCollection.on('add', this.renderItem, this);
            }

            if (!this.collection.is_fetched) {
                this.collection.fetch({
                    add: true,
                    reset: true,
                    update: true
                });
            }else{
                this.renderItems(this.collection);
            }

            this.collection.on('add', this.renderItem, this);
            this.collection.on('reset', this.reset, this);
            //                Events.on("scroll:end", this.nextPage, this);

            this.collection.on('fetchSuccess', function(resp, collection) {

                if (this.filters.get("start") + this.filters.get("limit") > resp.count) {
                    $(".no-more-items", this.$el).show();
                    $this.hasMoreItems = false;
                    $('.load-more').hide();
                    $(".no-items", this.$el).hide();
                }
                else {
                    $this.hasMoreItems = true;
                    $('.load-more').show();
                    $(".no-items", this.$el).hide();
                    $('.no-more-items').hide();
                }

                if (resp.count) {
                    $('.count').html(_.translate(resp.count.toString(), true));
                }

                if (this.filters.get('start') != 0)
                    $('.previous-page').removeClass('disabled');

                if ((this.filters.get('limit') + this.filters.get('start')) < resp.count) {
                    $('.load-more').show();
                }

                $this.renderItems($this.collection);
                //                    $this.afterRenderItems();
                $this.updatePaging();

            });

            $(window).scroll(this.onScroll);
        },
        reset: function() {
            _.each(this.items, function(itemView) {
                itemView.dispose();
            });

            this.items = [];
        },
        render: function(fields) {
            this.$el.html(this.template({
                labels: Labels,
                type: this.options.type || false
            }));

            return this;
        },
        afterRender: function() {
            this.initTable();
            this.createFilter();
        },
        renderItems: function() {
            var $this = this;
            this.reset();
            this.collection.each(function(model) {
                $this.renderItem(model)
            })
            //                this.afterRenderItems();
        },
        afterRenderItems: function() {
            if (this.collection.length == 0)
                return;
        },
        renderItem: function(model, index) {

            var item_view = new ItemView({
                model: model,
                index: index,
                type: this.options.type || model.get("type"),
                callee: this
            });

            this.items.push(item_view);
            item_view.render();
        },
        nextPage: function() {

            if (!this.hasMoreItems)
                return;

            this.paging = true;

            this.collection.nextPage().fetch({
                add: true,
                reset: true,
                update: true
            });
        },
        getNewsByType: function(e) {
            var el = $(e.target),
                type = el.data("type");

            if (!el.hasClass("selected")) {
                $(".second-navbar .selected", this.$el).removeClass("selected");
                el.addClass("selected");
                this.collection.filters.set("type", type);
            }

        },
        showEditPopup: function(e) {
            var itemId = $(e.target).parents('.itme-wrapper').attr("id");
            this.itemModel = (this.target == "categories") ? this.categoriesCollection.get(itemId) : this.collection.get(itemId);
            this.popForm(this.itemModel);
        },
        showAddPopup: function() {
            if (this.target == "categories") {
                this.itemModel = new CategoryModel({
                    target: this.options.type.slice(0, -1)
                })
            } else {
                this.itemModel = new this.ItemModel();
            }
            this.popForm(this.itemModel);
        },
        popForm: function(model) {

            var $this = this;
            if (this.options.type == "users") {
                var formView = new UserForm({
                    collection: this.collection,
                    model: model
                });
                $("#new-item-modal", $this.$el).html(formView.render().$el);
                formView.afterRender();
            }
            else if (this.options.type == "news") {
                var formView = new NewsForm({
                    collection: this.collection,
                    model: model
                });
                $("#new-item-modal", $this.$el).html(formView.render().$el);
            }
            else if (this.options.type == "keywords") {
                var formView = new KeywordForm({
                    collection: this.collection,
                    model: model
                });
                $("#new-item-modal", $this.$el).html(formView.render().$el);
                formView.afterRender();
            }
            else {
                require(['text!views/templates/' + this.options.type.slice(0, -1) + '_form.html'], function(FormTpl) {
                    var template = FormTpl,
                        tpl = _.template(template);

                    $("#new-item-modal", $this.$el).html(tpl(_.extend(model.toJSON(), {
                        target: $this.target || $this.options.type,
                        labels: Labels,
                        categories: ($this.categoriesCollection) ? $this.categoriesCollection.toJSON() : []
                    })));
                });
            }

            $("#new-item-modal", this.$el).modal({
                backdrop: "static"
            });
        },
        validate: function() {
            var validate = true;

            $(".active form [required]").each(function(index, el) {
                if ($(el).val() == "") {
                    validate = false;
                    $(el).parent().addClass("error");
                }

            });
            !validate && $(".rquired_error").css("visibility", "visible");
            return validate;
        },
        updateItem: function() {
            this.createNewItem();
        },
        createNewItem: function() {
            $(".error").removeClass("error");
            $(".rquired_error").css("visibility", "hidden");
            if (!this.validate())
                return;

            var data = $(".active form", this.$el).serializeJSON(),
                $this = this,
                mode = (this.itemModel.get("id")) ? "update" : "create";

            $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);

            data.images = this.images;

            this.itemModel.save(data, {
                silent: true,
                success: function(model) {
                    if (mode == "create") {
                        if ($this.target == "categories")
                            $this.categoriesCollection.add(model);
                        else
                            $this.collection.add(model);
                    } else {
                        model.trigger("change");
                    }
                    $("#new-item-modal", this.$el).modal('hide');
                }
            });

        },
        changeTab: function(e) {
            var el = $(e.target);
            this.target = el.data("target");

            if (el.hasClass("selected"))
                return;

            if (this.target == "categories") {
                $("." + this.options.type + '-wrapper').hide();
                $(".categories-wrapper").show();
                this.renderItems(this.categoriesCollection);
            } else {
                $("." + this.options.type + '-wrapper').show();
                $(".categories-wrapper").hide();
                this.renderItems(this.collection);
            }

            $(".second-navbar .selected", this.$el).removeClass("selected");
            el.addClass("selected");
        },
        setCategory: function(e) {
            var categoryId = $(e.target).val();
            if (categoryId)
                this.itemModel.set("category", this.categoriesCollection.get(categoryId));
        },
        search: function(e) {
            var input = $(e.target),
                holder = $(".items-wrapper", this.$el);

            var regex = new RegExp(input.val(), 'gi');

            $('.itme-wrapper', holder).each(function(idx, elem) {
                var email = $(".title", $(elem)).text();
                if ($.trim(email).match(regex)) {
                    $(elem).show()
                } else {
                    $(elem).hide();
                }
            });
        },
        onScroll : function(){
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                this.collection.autoPagination &&  this.collection.nextPage();
                return false;
            }
        },
    });
    return ItemsListView
});
