
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!views/templates/list.html',
    'text!views/templates/cause.html',
    'text!views/templates/coordination.html',
    'text!views/templates/level.html',
    'text!views/templates/region.html',
    'text!views/templates/resource.html',
    'text!views/templates/type.html',
    'text!views/templates/user.html',
    'text!views/templates/news.html',
    'text!views/templates/keyword.html',
    'text!views/templates/contact_group.html',
    'i18n!nls/labels',
    "jquery-helper",
], function($, _, Backbone, BaseView, ListTpl,
    CauseTpl, CoordinationTpl, LevelTpl, RegionTpl, ResourceTpl,
    TypeTpl, UserTpl, NewsTpl, KeywordTpl, ContactGroupTpl, Labels) {

    var ItemView = BaseView.extend({
        className: "itme-wrapper",
        events: {
            'click .switch-mode': 'switchViewMode',
            'click .close-evaluation': 'showAlert',
            'click .btn-danger': 'deleteItem',
            'click .hide-alert': 'hideAlert'
        },
        initialize: function() {

            this.callee = this.options.callee;

            switch (this.options.type) {
                case "causes":
                    this.template = _.template(CauseTpl);
                    break;

                case "coordinations" :
                    this.template = _.template(CoordinationTpl);
                    break;

                case "levels" :
                    this.template = _.template(LevelTpl);
                    break;

                case "regions" :
                    this.template = _.template(RegionTpl);
                    break;

                case "resources" :
                    this.template = _.template(ResourceTpl);
                    break;

                case "types" :
                    this.template = _.template(TypeTpl);
                    break;

                case "users" :
                    this.template = _.template(UserTpl);
                    break;

                case "news" :
                    this.template = _.template(NewsTpl);
                    break;

                case "keywords" :
                    this.template = _.template(KeywordTpl);
                    break;

                case "contact-groups" :
                    this.template = _.template(ContactGroupTpl);
                    break;
            }

            this.model.on("change", this.render, this);
        },
        render: function(fields) {
            this.$el.html(this.template(_.extend(this.model.toJSON(), {
                labels: Labels
            })));

            if (!$("#" + this.model.get("id")).length) {
                this.$el.attr("id", this.model.get("id"));
                var holder = this.getItemHolder();
                holder.append(this.$el);
            }

            this.afterRender();
            return this;
        },
        getItemHolder: function() {
            var holder;

            if (this.model.get("type") == "category") {
                holder = $(".categories-wrapper", this.callee.$el);
            }
            else {
                if (this.callee.categoriesCollection) {
                    if ($("#category-" + this.model.get("category").id).length)
                        holder = $("#category-" + this.model.get("category").id);
                    else {
                        holder = $("<div id='category-" + this.model.get("category").id + "'><h3 class='category-title'>" + this.model.get("category").title + "</h3></div>")
                        $(".items-wrapper", this.callee.$el).append(holder);
                    }

                } else {
                    holder = $(".items-wrapper", this.callee.$el);
                }
            }

            return holder;
        },
        afterRender: function() {
            if (this.model.get("target") == "reports")
                this.initMap();

            $(".hover-tooltip", this.$el).tooltip();
        },
        switchViewMode: function() {
            var switch_mode = (this.model.get("viewMode") == "view") ? "edit" : "view";
            this.model.set("viewMode", switch_mode);
        },
        initMap: function() {
            var location = [this.model.get('latitude'), this.model.get('longitude')];
            $(".gmap3", this.$el).gmap3({
                marker: {
                    latLng: location,
                    options: {
                        draggable: false
                    }
                },
                map: {
                    options: {
                        center: location,
                        zoom: 15,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        navigationControl: true,
                        scrollwheel: true,
                        streetViewControl: true,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_TOP,
                            style: "SMALL"
                        },
                        panControl: false,
                        panControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_TOP,
                            style: "SMALL"
                        }
                    }
                }
            });
        },
        showAlert: function() {
            $(".alert", this.$el).removeClass("hidden");
        },
        hideAlert: function() {
            $(".alert", this.$el).addClass("hidden");
        },
        deleteItem: function() {
            var $this = this;
            this.model.destroy({
                success: function() {
                    $this.remove();
                },
                error: function() {
                    var msg = Labels.report_that;
                    msg += " «";
                    msg += Labels[$this.options.type];
                    msg += " »";
                    msg += Labels.forbbiden_delete;

                    alert(msg);
                }
            })
        }
    });

    return ItemView;
});
