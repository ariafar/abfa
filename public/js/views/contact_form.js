
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!views/templates/contact_form.html',
    'i18n!nls/labels',
    'jquery-helper',
], function($, _, Backbone, BaseView, FormTpl, Labels) {

    var ItemView = BaseView.extend({
        className: "modal-dialog contact-form",
        events: {
            'click #image_upload_btn': 'openSelectionFile',
            'change #file': 'setImage',
            'click .create-contact': "createContact"
        },
        initialize: function(options) {
            this.selectedGroup = options.selectedGroup;
            this.render();
        },
        render: function() {
            this.template = _.template(FormTpl);
            this.$el.html(this.template(_.extend(this.model.toJSON(), {
                labels: Labels
            })));
            return this;
        },
        setImage: function(e) {
            var input = $('#file')[0];

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.contact-form .user-image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        },
        openSelectionFile: function() {
            $("#file").trigger("click");
        },
        validate: function() {
            var validate = true;

            $("[required]").each(function(index, el) {
                if ($(el).val() == "") {
                    validate = false;
                    $(el).parent().addClass("error");
                }
            });

            !validate && $(".rquired_error").css("visibility", "visible");

            return validate;
        },
        createContact: function() {
            $(".error").removeClass("error");
            $(".rquired_error").css("visibility", "hidden");
            if (!this.validate())
                return;
            var data = $(".active form", this.$el).serializeJSON(),
                $this = this;
            $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);

            this.model.set("group_id", this.selectedGroup.get("id"));
            this.model.save(data, {
                silent: true,
                success: function(model) {
                    $this.saveImage();
                }
            });
        },
        saveImage : function() {
            var fd = new FormData(),
                file = $('#file')[0].files[0];
            if (file) {
                fd.append('photo', file);
                this.model.saveImage(fd);
            }

            this.onSave();
        },
        onSave: function() {
            this.collection.get(this.model.get("id")) && this.collection.add(model);
            $(".modal").modal('hide');
        },
    });

    return ItemView;
});
