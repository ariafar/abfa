
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!views/templates/keyword_form.html',
    'collections/keywords',
    'models/keyword',
    'i18n!nls/labels',
    'jquery-helper',
    'tagit'

    ], function ($, _, Backbone, BaseView, FormTpl, KeywordsCollection, KeywordModel, Labels, Tagit) {

        var ItemView = BaseView.extend({
            
            className : "modal-dialog keyword-form",
            
            events : {
                'click #image_upload_btn'       : 'openSelectionFile',
                'click .create-item'            : 'createUser',
                'click .update-item'            : 'createUser'
            },
            
            initialize : function() {
                if(!this.model){
                    this.model = new KeywordModel();
                }
                this.keywordsCollection = new KeywordsCollection([]);
                this.keywordsCollection.fetch({
                    async : false
                });
            },

            render : function() {
                this.template = _.template(FormTpl);
                
                this.$el.html(this.template(_.extend(this.model.toJSON(), {
                    labels  : Labels
                })));
                return this;
            },

            afterRender : function(){
                var availableTags = []
                ,initTags = [];

                this.keywordsCollection.each(function(item){
                    availableTags.push({
                        label : item.get("tag"),
                        value : item.get("tag")
                    })
                });
                
                if(this.model.get('levels') && this.model.get('levels').length ){
                    _.each(this.model.get('levels'), function(item){
                        initTags.push({
                            label : item.tag,
                            value : item.tag
                        });

                        availableTags = $.grep(availableTags, function(opt ,index){
                            return !( item.id == opt.value);
                        });
                    })
                }

                $('#display-aliases').tagit({
                    initialTags     : initTags,
                    tagSource       : function(request, response){
                        var re = new RegExp(request.term , "i");
                        var array = $.grep(availableTags, function(opt ,index){
                            return (re.test(opt.label));
                        });
                        response (array);
                    },
                    sortable        : true,
                    allowNewTags    : false,
                    minLength       : 0,
                    tagsChanged : function(tagValue, action, element){
                        if(action == "added"){
                            $('#disply-fields-div').hasClass("error") && $('#disply-fields-div').removeClass("error");
                            availableTags = $.grep(availableTags, function(opt ,index){
                                return !(opt.label == tagValue);
                            });
                        }

                        if(action == "popped")
                            availableTags.push({
                                label : element.label,
                                value : element.value
                            })
                    }
                });
            },

            openSelectionFile : function(){
                $("#file").trigger("click");
            },

            validate : function(){
                var validate =  true;

                $(".active form [required]").each(function(index, el){
                    if($(el).val() == ""){
                        validate =  false;
                        $(el).parent().addClass("error");
                    }
                });
                
                if($('#display-levels', this.$el).tagit("tags").length == 0){
                    validate =  false;
                    $('#display-levels', this.$el).parent().addClass("error");
                }
                !validate && $(".rquired_error").css("visibility", "visible");
                return validate;
            },

            createUser : function(){
                $(".error").removeClass("error");
                $(".rquired_error").css("visibility", "hidden");
                $(".error_msg").css("visibility", "hidden");
                
                if(!this.validate())
                    return;

                $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);
                
                var data =  _.extend(this.options.model.toJSON(), $("form", this.$el).serializeJSON()),
                levels = "",
                $this = this,
                mode = this.options.model.get("id") ? "edit" : "create";

                data.displayName =  data.firstname + " " + data.lastname;
                var fd = new FormData();
                fd.append( 'file', $('#file')[0].files[0] );
                for(key in data){
                    if (data[key])
                        fd.append(key, data[key]);
                }

                var tags = $('#display-levels', this.$el).tagit("tags");
                if(tags.length){
                    _.each(tags, function(tag, index){
                        levels += tag.value + ",";;
                    });
                    
                    fd.append('levels', levels.replace(/,+$/, ""));
                }
                
                $.ajax({
                    type: "POST",
                    url: "/api/users" + ( (this.options.model && this.options.model.get("id")) ? "/" + this.options.model.get("id") : '') ,
                    processData: false,
                    contentType: false,
                    data: fd,
                    success: function (obj) {
                        if(mode == "edit")
                            $this.options.model.set(obj);
                        else
                            $this.collection.add(obj);
                        $("#new-item-modal").modal('hide');
                    },
                    error : function(error){
                        $('.hold-on-loading').hide();
                        $(".modal-footer .btn:not(.btn-default)").attr("disabled", false);
                        for (key in error){
                            var el = $("[name=" + key + "]").parents(".control-group");
                            el.addClass("error");
                            $("span", el).css("visibility", "visible");
                        }
                    }
                });

                return false;
            }
        });

        return ItemView;
    });
