define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var ItemModel = Backbone.Model.extend({
        defaults: {
            'id': null,
            'title': null
        },
        url: function() {
            return '/api/contact-groups/' + this.get("group_id") + "/users/" + this.get("user_id")
        }
    });
    return ItemModel;
});


