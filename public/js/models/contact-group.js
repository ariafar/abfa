define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null
            },

            url : function(){
                return "api/contact-groups" + ( (this.get("id")) ? '/' + this.get("id") : '');
            }
        });
        return ItemModel;
    });


