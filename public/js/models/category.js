define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null,
                'unit'          : null
            },
            url : function(){
                return "api/" + this.get("target") + "_categories"  + ( (this.get("id")) ? '/' + this.get("id") : '');
            },
            parse : function(obj){
                obj.type = "category";
                return obj;
            }
        });
        return ItemModel;
    });


