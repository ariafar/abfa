define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null,
                'shortDesc'     : null,
                'longDesc'      : null,
                'photos'        : []
            },

            url : function(){
                return "api/news" + ( (this.get("id")) ? '/' + this.get("id") : '');
            },
            parse : function(obj){
                if(!obj.deleted)
                    return obj
            }

        });
        return ItemModel;
    });


