define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            
            defaults: {
                'tag'           : null,
                'aliases'       : null
            },

            url : function(){
                return "api/keywords" + ( (this.get("tag")) ? '/' + this.get("tag") : '');
            },
            
            parse : function(obj){
                obj.id = this.cid;
                return obj;
            }
        });
        
        return ItemModel;
    });


