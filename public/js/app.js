define([
    'jquery',
    'jquery-ui',
    'backbone',
    'underscore',
    'models/user',
    'collections/users',
    'moment',
//    'pwt-date',
//    'pwt-datepicker.min',
], function($, jquery_ui, Backbone, _, UserModel, UsersCollection) {

    window.currentUser = new UserModel();
    if (window.current_user) {
        window.currentUser.set(window.current_user)
    }

    window.usersCollection = new UsersCollection([]);
    window.usersCollection.fetch({
        async: false
    });

    $('body').click(function(e) {
        if (!$(e.target).closest(".popover-toggle").length && !$(e.target).closest(".popover").length) {
            $('.popover-toggle').popover('destroy');
            $(".popover").remove();
        }
    })

    var application = function() {
        return {
            Lang: {
                code: "fa",
                defaultCode: "fa",
                translate: function convert(string, seprate) {

                    if (string && string != "NULL") {
                        var string = string.toString();

                        if (seprate) {
                            while (/(\d+)(\d{3})/.test(string.toString())) {
                                string = string.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                            }
                        }

                        if (string) {
                            var persian_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
                            string = string.replace(/0/g, persian_numbers[0]);
                            string = string.replace(/1/g, persian_numbers[1]);
                            string = string.replace(/2/g, persian_numbers[2]);
                            string = string.replace(/3/g, persian_numbers[3]);
                            string = string.replace(/4/g, persian_numbers[4]);
                            string = string.replace(/5/g, persian_numbers[5]);
                            string = string.replace(/6/g, persian_numbers[6]);
                            string = string.replace(/7/g, persian_numbers[7]);
                            string = string.replace(/8/g, persian_numbers[8]);
                            string = string.replace(/9/g, persian_numbers[9]);
                        }
                        return string;
                    }
                    else
                        return string;
                },
                getNumber: function(value) {
                    value = value.replace(/,/g, "");

                    if (value) {
                        value = value.replace(/۰/g, 0);
                        value = value.replace(/۱/g, 1);
                        value = value.replace(/۲/g, 2);
                        value = value.replace(/۳/g, 3);
                        value = value.replace(/۴/g, 4);
                        value = value.replace(/۵/g, 5);
                        value = value.replace(/۶/g, 6);
                        value = value.replace(/۷/g, 7);
                        value = value.replace(/۸/g, 8);
                        value = value.replace(/۹/g, 9);
                    }

                    return value;
                }

            },
            Date: {
                getTimeStamp: function(date) {
                    var d = new Date(date.replace(/-/g, '/'));
                    return d.getTime();
                }
            },
            //                currentUser : currentUser,

            processScroll: function() {
                var $win = $(window)
                    , $nav = $('#navbar-collapse')
                    , navTop = 32
                    , isFixed = 0
                    , isAddH = false;

                processScroll2();
                $win.unbind('scroll', processScroll2);
                $win.bind('scroll', processScroll2);

                function processScroll2() {
                    var i, scrollTop = $win.scrollTop();

                    if (scrollTop >= navTop) {
                        $('#bulletin-toolbar').show();
                    }
                    else if (scrollTop <= navTop) {
                        $('#bulletin-toolbar').hide();
                    }
                }
            },
            ajaxSetup: function() {
                $.ajaxSetup({
                    beforeSend: function(jqXHR, settings) {
                        $('.hold-on-loading').show();
                    },
                    complete: function() {
                        setTimeout(function() {
                            $('.hold-on-loading').hide();
                        }, 1000)

                    }
                });
            }
        }
    }

    return application();
});
