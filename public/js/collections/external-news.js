define([
    'jquery',
    'underscore',
    'backbone', 
    'app',
    'base/collection/collection',
    'models/external-news',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var CausesCollection = BaseCollections.extend({
        model : ItemModel,

        url : function(){

            return '/api/external-news/' + this.type;
        }

    });
    return CausesCollection;
});
