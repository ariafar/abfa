define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/collection/collection',
    'models/user',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var UsersCollection = BaseCollections.extend({
        model: ItemModel,
        url: function() {
            return '/api/users';
        },
        parse: function(resp) {
            var items = [];
            _.each(resp, function(item) {
                item.label = item.firstname + " " + item.lastname;
//                item.value = item.id;
                if (item.type != "ADMIN")
                    items.push(item);
            });
            return items;
        }

    });
    return UsersCollection;
});
