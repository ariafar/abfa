define([
    'jquery',
    'underscore',
    'backbone', 
    'app',
    'base/collection/collection',
    'models/contact',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var CausesCollection = BaseCollections.extend({
        model : ItemModel,

        url : function(){
            return '/api/contact-groups/' + this.groupId + "/contacts";
        },
        
        parse : function(resp){
            var list = [],
                $this = this;
            
            _.each(resp.items,function(obj){
                obj.group_id = $this.groupId;
                list.push(obj);
            });
            
            return list;
        }

    });
    return CausesCollection;
});
