define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/collection/collection',
    'models/keyword',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var KeywordsCollection = BaseCollections.extend({
        model: ItemModel,
        url: function() {
            return '/api/keywords';
        },
        parse: function(resp) {
            return resp;
        },
    });
    return KeywordsCollection;
});
