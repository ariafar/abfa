define([
    'jquery',
    'underscore',
    'backbone', 
    'app',
    'base/collection/collection',
    'models/contact-group',
], function($, _, Backbone, App, BaseCollections, ItemModel) {
    var CausesCollection = BaseCollections.extend({
        model : ItemModel,

        url : function(){
            return '/api/contact-groups';
        }

    });
    return CausesCollection;
});
