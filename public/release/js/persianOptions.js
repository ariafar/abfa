define([
    'jquery',
    'backbone', 
    'underscore',  
    'pwt-date',
    'pwt-datepicker.min', 
    'stock-chart',
    'timeago'
    ], function($, Backbone, _, Backbone) {

    // Persian Options
    if(localStorage.getItem('locale') == "en-us"){
        Highcharts.dateFormats = {        
            FY: function (timestamp) {
                var date = new Date(timestamp);
                return date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();               
            },
            Y: function (timestamp) {
                var date = new Date(timestamp);

                return date.getFullYear();            
            }
        }

        return;
    }        

    $.extend($.timeago, {
        settings: {
          refreshMillis: 60000,
          allowPast: true,
          allowFuture: false,
          localeTitle: false,
          cutoff: 0,
          strings: {
            prefixAgo: null,
            prefixFromNow: null,
            suffixAgo: "پیش",
            suffixFromNow: "از حالا",
            seconds: "۱ دقیقه",
            minute: "۱ دقیقه",
            minutes: "%d دقیقه",
            hour: "۱ ساعت" ,
            hours: "%d ساعت",
            day: "دیروز",
            days: "%d روز",
            month: "۱ ماه",
            months: "%d ماه",
            year: "۱ سال",
            years: "%d سال",
            wordSeparator: " ",
            numbers: []
          }}
    });

    // HighChart Options

    var dateTimeLabelFormats = {
                    second: '%Y/%m/%d<br/>%H:%M:%S',
                    minute: '%Y/%m/%d<br/>%H:%M',
                    hour: '%Y/%m/%d<br/>%H:%M',
                    day: '%Y/%m/%d',
                    week: '%Y/%m/%d',
                    month: '%Y/%m',
                    year: '%Y'
    };

    Highcharts.setOptions({
        plotOptions:{
            candlestick : {dateTimeLabelFormats : dateTimeLabelFormats},
            line : {dateTimeLabelFormats : dateTimeLabelFormats}
        },
        lang: {
            loading: '',
            months: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر',
                        'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],          
            shortMonths : ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر',
                        'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],                                 
            weekdays: ['یکشنبه', 'دوشنبه', 'سه شنبه', 'چهار شنبه', 'پنج شنبه', 'جمعه', 'شنبه'],
            decimalPoint: '.',
            numericSymbols: ['k', 'M', 'G', 'T', 'P', 'E'], 
            resetZoom: 'بزرگنمایی اولیه',
            resetZoomTitle: 'بزرگنمایی 1:1',
            thousandsSep: ',',
            rangeSelectorZoom: 'محدوده',
            rangeSelectorFrom: 'از:',
            rangeSelectorTo: 'تا:'
        }});

       Highcharts.Axis.prototype.defaultOptions.dateTimeLabelFormats = {
            second: '%Y/%m/%d<br/>%H:%M:%S',
            minute: '%Y/%m/%d<br/>%H:%M',
            hour: '%Y/%m/%d<br/>%H:%M',
            day: '%Y/%m/%d',
            week: '%Y/%m/%d',
            month: '%Y/%m',
            year: '%Y'
       };

        Highcharts.RangeSelector.prototype.defaultButtons = [{
                type: 'day',
                count: 1,
                text: '۱روز'
            },{
                type: 'week',
                count: 1,
                text: '۱هفته'
            },{
                type: 'month',
                count: 1,
                text: '۱ماه'
            }, {
                type: 'month',
                count: 3,
                text: '۳ماه'
            }, {
                type: 'ytd',
                text: 'سال جاری'
            }, {
                type: 'year',
                count: 1,
                text: '۱ سال'
            }, {
                type: 'all',
                text: 'همه'
            }];   

        Highcharts.dateFormats = {        
            FY: function (timestamp) {
                var date = new Date(timestamp),
                   pDate = new persianDate(date).format("YY/M/D");
                
                return pDate;
            }, 
            Y: function (timestamp) {
                var date = new Date(timestamp);

                return new persianDate(date).format('YY/M/D').split('/')[0];            
            },
            m : function(timestamp){
                var date = new Date(timestamp);

                return new persianDate(date).format('YY/M/D').split('/')[1]; 
            },
            d : function(timestamp){
                var date = new Date(timestamp);

                return new persianDate(date).format('YY/M/D').split('/')[2]; 
            }
        }
  });