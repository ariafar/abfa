define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var ItemModel = Backbone.Model.extend({
            defaults: {
                'id'            : null,
                'title'         : null,
                'description'   : null
            },

            url : function(){
                return "api/causes" + ( (this.get("id")) ? '/' + this.get("id") : '');
            }
        });
        return ItemModel;
    });


