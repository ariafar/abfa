<?php
return array(
    'controller_plugins' => array(
        'invokables' => array(
            'ServerUrl' => 'Application\Controller\Plugin\ServerUrl',
        )
    ),
    'router' => array(
        'routes' => array(
            'receive-sms' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/receive-sms',
                    'defaults' => array(
                        'controller' => 'Application\Controller\ReceiveSms',
                        'action' => 'receive'
                    ),
                ),
            ),

            'webservice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/webservice/create-event',
                    'defaults' => array(
                        'controller' => 'Application\Controller\WebService',
                        'action' => 'create'
                    ),
                ),
            ),

            'image' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/image/[:hash]',
                    'constraints' => array(
                        'hash' => '.*',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Image',
                        'action' => 'get'
                    ),
                ),
            ),

            'api' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/api',
                ),
                'may_terminate' => true,
                'child_routes' => array(

					'abfa122'  => array(
						'type' => 'literal',
                        'options' => array(
                            'route' => '/abfa122',
                        ),
						'may_terminate' => true,

						'child_routes' => array(
							'register' => array(
								'type' => 'literal',
								'options' => array(
									'route' => '/register',
									'defaults' => array(
										'controller' => 'Application\Controller\Abfa122',
										'action' => 'register'
									),
								)
							),
							'getEvents' => array(
								'type' => 'literal',
								'options' => array(
									'route' => '/get-events',
									'defaults' => array(
										'controller' => 'Application\Controller\Abfa122',
										'action' => 'getEvents'
									),
								)
							),

							'updateEvent' => array(
								'type' => 'literal',
								'options' => array(
									'route' => '/update-event',
									'defaults' => array(
										'controller' => 'Application\Controller\Abfa122',
										'action' => 'updateEvent'
									),
								)
							),

							'uploadPhoto' => array(
								'type' => 'literal',
								'options' => array(
									'route' => '/upload-photo',
									'defaults' => array(
										'controller' => 'Application\Controller\Abfa122',
										'action' => 'uploadPhoto'
									),
								)
							),

						)
					),

                    'shifts' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/shifts[/[:id]]',
                            'constraints' => array(
                                'id' => '.*',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\Shift'
                            ),
                        ),
                    ),

                    'contacts' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/contacts',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Contact'
                            ),
                        ),
                    ),

                    'contact_groups' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/contact-groups[/[:id]]',
                            'constraints' => array(
                                'id' => '.*',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\ContactGroup'
                            ),
                        ),
                    ),

                    'contact_group_contacts' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/contact-groups/[:group_id]/contacts[/[:id]]',
                            'constraints' => array(
                                'group_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\ContactGroupContact'
                            ),
                        ),
                    ),

                    'contact_group_contact_photo' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/contact-groups/[:group_id]/contacts/[:contact_id]/photo',
                            'constraints' => array(
                                'group_id' => '[0-9]+',
                                'contact_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\ContactGroupContactPhoto'
                            ),
                        ),
                    ),

                    'contact_group_users' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/contact-groups/[:group_id]/users[/[:id]]',
                            'constraints' => array(
                                'group_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\ContactGroupUser'
                            ),
                        ),
                    ),

                    'keywords' => array (
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/keywords[/[:id]]',
                            'constraints' => array(
                                'id' => '.*',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\KeywordRest'
                            ),
                        ),
                    ),

                    'external_news' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/external-news[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\ExternalNews'
                            ),
                        ),
                    ),

                    'external_news_unpublished' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/external-news/unpublished',
                            'defaults' => array(
                                'controller' => 'Application\Controller\ExternalNews',
                                'action' => 'unpublished'
                            ),
                        ),
                    ),

                    'external_news_not_processed' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/external-news/not-processed',
                            'defaults' => array(
                                'controller' => 'Application\Controller\ExternalNews',
                                'action' => 'notProcessed'
                            ),
                        ),
                    ),

                    'external_news_published' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/external-news/published',
                            'defaults' => array(
                                'controller' => 'Application\Controller\ExternalNews',
                                'action' => 'published'
                            ),
                        ),
                    ),

                    'external_news_publish' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/external-news/[:id]/publish',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\ExternalNews',
                                'action' => 'publish'
                            ),
                        ),
                    ),

                    'external_news_unpublish' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/external-news/[:id]/unpublish',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\ExternalNews',
                                'action' => 'unpublish'
                            ),
                        ),
                    ),


                    'news' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/news[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\News'
                            ),
                        ),
                    ),
                    'news_photo' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/news/[:news_id]/photos[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'news_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\NewsPhoto'
                            ),
                        ),
                    ),
                    'events' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/events[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9a-zA-Z\-]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\EventRest'
                            ),
                        ),
                    ),
                    'event_photos' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/events/[:event_id]/photos[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'event_id' => '[0-9a-zA-Z\-]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\EventPhoto'
                            ),
                        ),
                    ),

                    'event_users' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/events/[:event_id]/users[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'event_id' => '[0-9a-zA-Z\-]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\EventUser'
                            ),
                        ),
                    ),

                    'event_coordinations' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/events/[:event_id]/coordinations[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'event_id' => '[0-9a-zA-Z\-]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\EventCoordination'
                            ),
                        ),
                    ),

                    'event_comments' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/events/[:event_id]/comments[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'event_id' => '[0-9a-zA-Z\-]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\EventComment'
                            ),
                        ),
                    ),

                    'event_resources' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/events/[:event_id]/resources[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'event_id' => '[0-9a-zA-Z\-]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\EventResource'
                            ),
                        ),
                    ),
                    'sync' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/sync',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Sync',
                                'action' => 'sync'
                            )
                        )
                    ),

                    'user_password' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/change-password',
                            'defaults' => array (
                                'controller' => 'Application\Controller\UserRest',
                                'action'     => 'changePassword'
                            )
                        )
                    ),

                    'user_verify' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/verify',
                            'defaults' => array (
                                'controller' => 'Application\Controller\UserRest',
                                'action'     => 'verify'
                            )
                        )
                    ),

                    'user_send_code' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/send-code',
                            'defaults' => array (
                                'controller' => 'Application\Controller\UserRest',
                                'action'     => 'sendCode'
                            )
                        )
                    ),

                    'me_photo' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/photo',
                            'defaults' => array (
                                'controller' => 'Application\Controller\UserPhoto',
                            )
                        )
                    ),

                    'user_photo' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/users/[:user_id]/photo',
                            'constraints' => array(
                                'user_id' => '[0-9]+',
                            ),
                            'defaults' => array (
                                'controller' => 'Application\Controller\UserPhoto',
                            )
                        )
                    ),

                    /*
                    'user_login' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/login',
                            'defaults' => array (
                                'controller' => 'Application\Controller\UserRest',
                                'action'     => 'login'
                            )
                        )
                    ),
                     */

                    'users' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/users[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\UserRest'
                            ),
                        ),
                    ),
                    'crud_items' => array(
                        'type' => 'regex',
                        'options' => array(
                            'regex' => '/(causes|coordinations|levels|regions|resources|types|type_categories|resource_categories)(/(?<id>[0-9]+))?',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Crud',
                            ),
                            'spec' => '/%section'
                        ),
                    )
                )
            ),

            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
            'Zend\Authentication\AuthenticationService' => 'zfcuser_auth_service',
        ),
        'factories' => array(
            'Application\Service\GcmService' => 'Application\Service\GcmServiceFactory',
            'Application\Service\SendSms' => 'Application\Service\CandooSmsFactory',
            'Hydrator' => 'Application\Service\HydratorFactory'
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Abfa122' => 'Application\Controller\Abfa122Controller',
            'Application\Controller\Sync'  => 'Application\Controller\SyncController',
            'Application\Controller\Crud'  => 'Application\Controller\CrudController',
            'Application\Controller\News'  => 'Application\Controller\NewsController',
            'Application\Controller\ExternalNews'  => 'Application\Controller\ExternalNewsController',
            'Application\Controller\NewsPhoto'  => 'Application\Controller\NewsPhotoController',
            'Application\Controller\EventRest'  => 'Application\Controller\EventRestController',
            'Application\Controller\EventPhoto' => 'Application\Controller\EventPhotoController',
            'Application\Controller\EventUser' => 'Application\Controller\EventUserController',
            'Application\Controller\EventCoordination' => 'Application\Controller\EventCoordinationController',
            'Application\Controller\EventComment' => 'Application\Controller\EventCommentController',
            'Application\Controller\EventResource' => 'Application\Controller\EventResourceController',
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\UserRest' => 'Application\Controller\UserRestController',
            'Application\Controller\UserPhoto' => 'Application\Controller\UserPhotoController',
            'Application\Controller\ReceiveSms' => 'Application\Controller\ReceiveSmsController',
            'Application\Controller\WebService' => 'Application\Controller\WebServiceController',
            'Application\Controller\Image'      => 'Application\Controller\ImageController',
            'Application\Controller\FeedCrawler' => 'Application\Controller\FeedCrawlerController',
            'Application\Controller\NewsCrawler' => 'Application\Controller\NewsCrawlerController',
            'Application\Controller\KeywordRest' => 'Application\Controller\KeywordRestController',
            'Application\Controller\Shift'  => 'Application\Controller\ShiftController',
            'Application\Controller\ShiftConsole'  => 'Application\Controller\ShiftConsoleController',
            'Application\Controller\Contact' => 'Application\Controller\ContactController',
            'Application\Controller\ContactGroup' => 'Application\Controller\ContactGroupController',
            'Application\Controller\ContactGroupUser' => 'Application\Controller\ContactGroupUserController',
            'Application\Controller\ContactGroupContact' => 'Application\Controller\ContactGroupContactController',
            'Application\Controller\ContactGroupContactPhoto' => 'Application\Controller\ContactGroupContactPhotoController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'fetch-feeds' => array(
                    'type'    => 'simple',
                    'options' => array(
                        'route'    => 'fetch-feeds',
                        'defaults' => array(
                            'controller' => 'Application\Controller\FeedCrawler',
                            'action'     => 'crawl'
                        )
                    )
                ),

                'fetch-news' => array(
                    'type'    => 'simple',
                    'options' => array(
                        'route'    => 'fetch-news',
                        'defaults' => array(
                            'controller' => 'Application\Controller\NewsCrawler',
                            'action'     => 'crawl'
                        )
                    )
                ),

                'send-notifications' => array(
                    'type'    => 'simple',
                    'options' => array(
                        'route'    => 'send-notifications',
                        'defaults' => array(
                            'controller' => 'Application\Controller\ShiftConsole',
                            'action'     => 'sendNotification'
                        )
                    )
                )
            ),
        ),
    ),

    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Application/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(
                    'Application\Entity' => 'application_entities',
                )
            ),
        ),

    ),
);
