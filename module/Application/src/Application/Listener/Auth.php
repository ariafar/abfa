<?php

namespace Application\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Http\Response;
use Zend\Http\Request as HttpRequest;

class Auth
{
    /**
     * @param MvcEvent $e
     */
    public static function onRoute(MvcEvent $e)
    {
        if (!$e->getRequest() instanceof HttpRequest) {
            return;
        }

        $token   = $e->getRequest()->getQuery('access_token', null);
        $token   = $e->getRequest()->getPost('access_token', $token);
        $headers = $e->getRequest()->getHeaders();

        if (empty($token)) {
            return;
        }

        $sm = $e->getApplication()->getServiceManager();
        $em = $sm->get('Doctrine\ORM\EntityManager');

        $uToken = $em->getRepository('Application\Entity\UserToken')->findOneBy(
            array('token' => $token)
        );

        if (empty($uToken)) {
            return;
        }

        $user = $uToken->getUser();

        if ($user) {
            $user = $em->find('Application\Entity\User', $user->getId());
            $authService = $sm->get('Zend\Authentication\AuthenticationService');
            $authService->getStorage()->write($user);
        }
    }
}
