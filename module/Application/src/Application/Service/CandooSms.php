<?php
namespace Application\Service;

use Zend\Stdlib\AbstractOptions;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZendService\Google\Exception\RuntimeException;

class CandooSms extends AbstractOptions implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    protected $username;

    protected $password;

    protected $domain;

    protected $baseUrl = "http://panel.candoosms.com/services/?wsdl";

    protected $senderNumber;

    protected $error_max_value = 1000;

    private $outputSeparator = "\n";

    private $errors;

    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function __construct($options)
    {
        parent::__construct($options);
    }

    public function getClient()
    {

        $client = new \SoapClient($this->getBaseUrl());

        return $client;
    }

    public function send($phones, $message)
    {
        if (!is_array($phones)) {
            $phones = array($phones);
        }

        $method = "Send";
        $params = array(
            'username'  => $this->getUsername(),
            'password'  => $this->getPassword(),
            'srcNumber' => $this->getSenderNumber(),
            'body'      => $message,
            'destNo'    => $phones,
            'flash'     => 0
        );

        $client = $this->getClient();
        try {
            $result = $client->__call($method, $params);
        } catch(\SoapFault $fault) {
            return false;
        }

        return true;
    }

    public function getLatestMessages()
    {
        $method = "getAllMessages"; // name of the service
        $numberOfMessasges = 10; // [FILL] number of the messages to fetch

        // creating the parameter array
        $params = array(
            'domain' => $this->getDomain(),
            'numberOfMessages' => $numberOfMessasges
        );

        // sending the request via webservice
        $response = $this->getClient()->__call($method,$params);

        var_dump($response);
        if(count($response) == 0){
            echo "No new message".$this->outputSeparator;
        } else{
            // display the incoming message(s)
            foreach($response as $result){
                echo "Message:".$this->outputSeparator;
                var_dump($result);
            }
        }
    }

    /**
     * Get username.
     *
     * @return username.
     */
    function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param username the value to set.
     */
    function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get password.
     *
     * @return password.
     */
    function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password.
     *
     * @param password the value to set.
     */
    function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get domain.
     *
     * @return domain.
     */
    function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set domain.
     *
     * @param domain the value to set.
     */
    function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Get baseUrl.
     *
     * @return baseUrl.
     */
    function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Set baseUrl.
     *
     * @param baseUrl the value to set.
     */
    function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Set senderNumber.
     *
     * @param senderNumber the value to set.
     */
    function setSenderNumber($senderNumber)
    {
        $this->senderNumber = $senderNumber;
    }

    /**
     * Get senderNumber.
     *
     * @return senderNumber.
     */
    function getSenderNumber()
    {
        return $this->senderNumber;
    }
}
