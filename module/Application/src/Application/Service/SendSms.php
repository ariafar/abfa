<?php
namespace Application\Service;

use Zend\Stdlib\AbstractOptions;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZendService\Google\Exception\RuntimeException;

class SendSms extends AbstractOptions implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    protected $username;

    protected $password;

    protected $domain;

    protected $baseUrl = "http://sms.magfa.com/services/urn:SOAPSmsQueue?wsdl";

    protected $senderNumber;

    protected $error_max_value = 1000;

    private $outputSeparator = "\n";

    private $errors;

    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function __construct($options)
    {
        parent::__construct($options);
    }

    public function getClient()
    {
        $client = new \SoapClient(
            $this->getBaseUrl(),
            array(
                'login'    => $this->getUsername(),
                'password' => $this->getPassword(),
                'features' => SOAP_USE_XSI_ARRAY_TYPE
                //,'trace' => true // Optional (debug)
            ));

        return $client;
    }

    public function send($phones, $message)
    {
        if (!is_array($phones)) {
            $phones = array($phones);
        }

        $method = "enqueue";
        $params = array(
            'domain'           => $this->getDomain(),
            'messageBodies'    => array($message),
            'recipientNumbers' => $phones,
            'senderNumbers'    => array($this->getSenderNumber())
        );

        $client  = $this->getClient();
        $response = $client->__call($method, $params);
        $result   = $response[0];

        if ($result <= $this->error_max_value) {
            return false;
            //echo "An error occured".$this->outputSeparator;
            //echo "Error Code : $result ; Error Title : " . $this->errors[$result]['title'] . ' {' . $this->errors[$result]['desc'] . '}'.$this->outputSeparator;
        } else {
            return true;
            //echo "Message has been successfully sent ; MessageId : $result".$this->outputSeparator;
        }
    }

    public function getLatestMessages()
    {
        $method = "getAllMessages"; // name of the service
        $numberOfMessasges = 10; // [FILL] number of the messages to fetch

        // creating the parameter array
        $params = array(
            'domain' => $this->getDomain(),
            'numberOfMessages' => $numberOfMessasges
        );

        // sending the request via webservice
        $response = $this->getClient()->__call($method,$params);

        var_dump($response);
        if(count($response) == 0){
            echo "No new message".$this->outputSeparator;
        } else{
            // display the incoming message(s)
            foreach($response as $result){
                echo "Message:".$this->outputSeparator;
                var_dump($result);
            }
        }
    }

    /**
     * Get username.
     *
     * @return username.
     */
    function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param username the value to set.
     */
    function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get password.
     *
     * @return password.
     */
    function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password.
     *
     * @param password the value to set.
     */
    function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get domain.
     *
     * @return domain.
     */
    function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set domain.
     *
     * @param domain the value to set.
     */
    function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Get baseUrl.
     *
     * @return baseUrl.
     */
    function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Set baseUrl.
     *
     * @param baseUrl the value to set.
     */
    function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Set senderNumber.
     *
     * @param senderNumber the value to set.
     */
    function setSenderNumber($senderNumber)
    {
        $this->senderNumber = $senderNumber;
    }

    /**
     * Get senderNumber.
     *
     * @return senderNumber.
     */
    function getSenderNumber()
    {
        return $this->senderNumber;
    }
}
