<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Event
 *
 * @ORM\Table(name="events", indexes={@ORM\Index(name="events_region_id_fk", columns={"region_id"}), @ORM\Index(name="events_level_id_fk", columns={"level_id"})})
 * @ORM\Entity
 *
 * @Form\Name("link")
 * @Form\Hydrator("Zend\Stdlib\Hydrator\ClassMethods")
 */
class Event
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="report_time", type="datetime", nullable=false)
     * @Form\Exclude
     */
    private $reportTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="happening_time", type="datetime", nullable=true)
     *
     * @Form\Required(true)
     */
    private $happeningTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_time", type="datetime", nullable=false)
     * @Form\Exclude
     */
    private $updateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="text", precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Required(false)
     */
    private $location;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=true, unique=false)
     *
     * @Form\Required(false)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=true, unique=false)
     *
     * @Form\Required(false)
     */
    private $latitude;

    /**
     * @var \Application\Entity\Level
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Level")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="level_id", referencedColumnName="id", nullable=true)
     * })
     *
     * @Form\Required(true)
     */
    private $level;

    /**
     * @var \Application\Entity\Region
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id", nullable=true)
     * })
     *
     * @Form\Required(true)
     */
    private $region;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Cause", inversedBy="event")
     * @ORM\JoinTable(name="event_causes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="cause_id", referencedColumnName="id")
     *   }
     * )
     */
    private $cause;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Type", inversedBy="event")
     * @ORM\JoinTable(name="event_types",
     *   joinColumns={
     *     @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     *   }
     * )
     */
    private $type;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     *
     * @Form\Exclude
     */
    private $creator;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", columnDefinition="ENUM('MOBILE','WEBSERVICE')", nullable=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"StringToUpper"})
     * @Form\Required(false)
     * @Form\Validator({"name" : "InArray", "options" : {"haystack" : {"MOBILE", "WEBSERVICE"}}})
     */
    private $source = "MOBILE";

    /**
     * @var boolean
     *
     * @ORM\Column(name="readonly", type="boolean", nullable=true)
     * @Form\Exclude()
     */
    private $readonly = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cause = new \Doctrine\Common\Collections\ArrayCollection();
        $this->type  = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Event
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Event
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Event
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Event
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set level
     *
     * @param \Application\Entity\Level $level
     * @return Event
     */
    public function setLevel(\Application\Entity\Level $level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return \Application\Entity\Level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set region
     *
     * @param \Application\Entity\Region $region
     * @return Event
     */
    public function setRegion(\Application\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Application\Entity\egion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Add cause
     *
     * @param \Application\Entity\Cause $cause
     * @return Event
     */
    public function addCause(\Application\Entity\Cause $cause)
    {
        $this->cause[] = $cause;

        return $this;
    }

    /**
     * Remove cause
     *
     * @param \Application\Entity\Cause $cause
     */
    public function removeCause(\Application\Entity\Cause $cause)
    {
        $this->cause->removeElement($cause);
    }

    /**
     * Get cause
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCause()
    {
        return $this->cause;
    }

    /**
     * Add type
     *
     * @param \Application\Entity\Type $type
     * @return Event
     */
    public function addType(\Application\Entity\Type $type)
    {
        $this->type[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \Application\Entity\Type $type
     */
    public function removeType(\Application\Entity\Type $type)
    {
        $this->type->removeElement($type);
    }

    /**
     * Get type
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get happeningTime.
     *
     * @return happeningTime.
     */
    function getHappeningTime()
    {
        return $this->happeningTime;
    }

    /**
     * Set happeningTime.
     *
     * @param happeningTime the value to set.
     */
    function setHappeningTime($happeningTime)
    {
        $this->happeningTime = $happeningTime;
    }

    /**
     * Get reportTime.
     *
     * @return reportTime.
     */
    function getReportTime()
    {
        return $this->reportTime;
    }

    /**
     * Set reportTime.
     *
     * @param reportTime the value to set.
     */
    function setReportTime($reportTime)
    {
        $this->reportTime = $reportTime;
    }

    /**
     * Set user
     *
     * @param \Application\Entity\User $user
     * @return Event
     */
    public function setCreator(\Application\Entity\User $user = null)
    {
        $this->creator = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\User", inversedBy="event")
     * @ORM\JoinTable(name="event_users",
     *   joinColumns={
     *     @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   }
     * )
     */
    private $user;

    /**
     * Add user
     *
     * @param \Application\Entity\User $user
     * @return Event
     */
    public function addUser(\Application\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Application\Entity\User $user
     */
    public function removeUser(\Application\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     *
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get updateTime.
     *
     * @return updateTime.
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set updateTime.
     *
     * @param updateTime the value to set.
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return Event
     */
    public function setSource($source)
    {
        $this->source = strtoupper($source);

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return strtoupper($this->source);
    }

    /**
     * Set readonly
     *
     * @param boolean $readonly
     * @return Event
     */
    public function setReadonly($readonly)
    {
        $this->readonly = (boolean) $readonly;

        return $this;
    }

    /**
     * Get readonly
     *
     * @return boolean
     */
    public function getReadonly()
    {
        return (boolean) $this->readonly;
    }
}
