<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventCoordination
 *
 * @ORM\Table(name="event_coordinations", indexes={@ORM\Index(name="coordination_id", columns={"coordination_id"}), @ORM\Index(name="event_id", columns={"event_id"})})
 * @ORM\Entity
 */
class EventCoordination
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\Coordination
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Coordination")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="coordination_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $coordination;

    /**
     * @var \Application\Entity\Event
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $event;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coordination
     *
     * @param \Application\Entity\Coordination $coordination
     * @return EventCoordination
     */
    public function setCoordination(\Application\Entity\Coordination $coordination = null)
    {
        $this->coordination = $coordination;

        return $this;
    }

    /**
     * Get coordination
     *
     * @return \Application\Entity\Coordination
     */
    public function getCoordination()
    {
        return $this->coordination;
    }

    /**
     * Set event
     *
     * @param \Application\Entity\Event $event
     * @return EventCoordination
     */
    public function setEvent(\Application\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Application\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Get description.
     *
     * @return description.
     */
    function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param description the value to set.
     */
    function setDescription($description)
    {
        $this->description = $description;
    }
}
