<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * News
 *
 * @ORM\Table(name="External_News", indexes={@ORM\Index(name="source_id", columns={"source_id"})})
 * @ORM\Entity
 *
 */
class ExternalNews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="headline", type="string", length=255, nullable=true)
     */
    private $headline;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="lead", type="string", length=255, nullable=false)
     */
    private $lead;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=255, nullable=false)
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false, unique=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var \Source
     *
     * @ORM\ManyToOne(targetEntity="Source")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="source_id", referencedColumnName="id")
     * })
     */
    private $source;


    /**
     * @var boolean
     * @ORM\Column(name="crawled", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crawled;

    /**
     * @var boolean
     * @ORM\Column(name="crawling", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $crawling;

    /**
     * @var boolean
     * @ORM\Column(name="valid", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $valid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = 'not_processed';


    /**
     * @ORM\ManyToMany(targetEntity="Keyword")
     * @ORM\JoinTable(name="News_Tags",
     *  joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="tag", referencedColumnName="tag")}
     * )
     */
    private $tags;

    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set headline
     *
     * @param string $headline
     * @return News
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;

        return $this;
    }

    /**
     * Get headline
     *
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set lead
     *
     * @param string $lead
     * @return News
     */
    public function setLead($lead)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return string
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return News
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return News
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return News
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return News
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set source
     *
     * @param \Application\Entity\Source $source
     * @return News
     */
    public function setSource(\Application\Entity\Source $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \Application\Entity\Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set crawled
     *
     * @param boolean $crawled
     * @return News
     */
    public function setCrawled($crawled)
    {
        $this->crawled = $crawled;

        return $this;
    }

    /**
     * Get crawled
     *
     * @return boolean
     */
    public function getCrawled()
    {
        return $this->crawled;
    }

    /**
     * Set crawling
     *
     * @param boolean $crawling
     * @return News
     */
    public function setCrawling($crawling)
    {
        $this->crawling = $crawling;

        return $this;
    }

    /**
     * Get crawling
     *
     * @return boolean
     */
    public function getCrawling()
    {
        return $this->crawling;
    }

    /**
     * Set valid
     *
     * @param boolean $valid
     * @return News
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid
     *
     * @return boolean
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Add tags
     *
     * @param \Application\Entity\Keyword $tags
     * @return News
     */
    public function addTag(\Application\Entity\Keyword $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \Application\Entity\Keyword $tags
     */
    public function removeTag(\Application\Entity\Keyword $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ExternalNews
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
