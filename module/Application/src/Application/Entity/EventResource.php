<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventResource
 *
 * @ORM\Table(name="event_resources", indexes={@ORM\Index(name="event_id", columns={"event_id"}), @ORM\Index(name="resource_id", columns={"resource_id"})})
 * @ORM\Entity
 */
class EventResource
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\Resource
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Resource")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="resource_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $resource;

    /**
     * @var \Application\Entity\Event
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $event;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", nullable=true)
     */
    private $count = 1;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resource
     *
     * @param \Application\Entity\Resource $resource
     * @return EventResource
     */
    public function setResource(\Application\Entity\Resource $resource = null)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return \Application\Entity\Resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Set event
     *
     * @param \Application\Entity\Event $event
     * @return EventResource
     */
    public function setEvent(\Application\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Application\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Get description.
     *
     * @return description.
     */
    function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param description the value to set.
     */
    function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get count.
     *
     * @return count.
     */
    function getCount()
    {
        return $this->count;
    }

    /**
     * Set count.
     *
     * @param count the value to set.
     */
    function setCount($count)
    {
        $this->count = $count;
    }
}
