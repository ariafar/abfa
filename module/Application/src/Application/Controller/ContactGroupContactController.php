<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\ContactGroup,
    Application\Entity\Contact,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class ContactGroupContactController extends AbstractRestfulController
{
    protected function getGroup()
    {
        $groupId = $this->params()->fromRoute('group_id');
        return $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\ContactGroup")
            ->find($groupId);
    }

    public function getList()
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Contact")
            ->findBy(
                array(
                    'group' => $group
                )
            );

        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $return   = array();
        foreach ($result as $row) {
            $_row = $hydrator->extract($row);

            unset($_row['group']);

            $return[] = $_row;
        }

        return new JsonModel(array('items' => $return));
    }

    public function get($id)
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Contact")
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $result = $this->getServiceLocator()->get('Hydrator')->extract($result);
        unset($result['group']);

        return new JsonModel($result);
    }

    public function create($data)
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();
        $entity   = new Contact();
        $form     = $builder->createForm($entity);

        $form->setHydrator($hydrator);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();
        $entity->setGroup($group);

        $em->persist($entity);
        $em->flush();

        return new JsonModel($this->extract($entity));
    }

    public function update($id, $data)
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $entity   = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Contact")
            ->find($id);

        if (!$entity) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);

        $preData = $hy->extract($entity);
        $data    = array_merge($preData, $data);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();

        $em->persist($entity);
        $em->flush();

        $result = $hy->extract($entity);
        unset($result['user']);
        return new JsonModel($result);
    }

    public function delete($id)
    {
        $group = $this->getGroup();
        if (!$group) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Contact")
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        try {
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->remove($result);
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->flush();
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('stats' => 'error'));
        }

        return new JsonModel(array('stats' => 'ok'));
    }
}
