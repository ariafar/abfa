<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class CrudController extends AbstractRestfulController
{
    private $sectionToTypeMap = array(
        'type_categories' => 'TypeCategory',
        'resource_categories' => 'ResourceCategory',
        'coordinations'  => 'Coordination',
        'causes' => 'Cause',
        'levels' => 'Level',
        'regions' => 'Region',
        'resources' => 'Resource',
        'types' => 'Type',
    );

    public function getList()
    {
        $path     = explode('/', trim($this->getRequest()->getUri()->getPath(), '/'));
        $section  = $path[1];
        $itemType = isset($this->sectionToTypeMap[$section]) ? $this->sectionToTypeMap[$section] : null;

        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\\Entity\\{$itemType}")
            ->findAll();
        $return = array();
        foreach ($result as $row) {
            $return[] = $this->extract($row);
        }

        return new JsonModel(array('items' => $return));
    }

    public function get($id)
    {
        $path     = explode('/', trim($this->getRequest()->getUri()->getPath(), '/'));
        $section  = $path[1];
        $itemType = isset($this->sectionToTypeMap[$section]) ? $this->sectionToTypeMap[$section] : null;

        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\\Entity\\{$itemType}")
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        return new JsonModel($this->extract($result));
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $path     = explode('/', trim($this->getRequest()->getUri()->getPath(), '/'));
        $section  = $path[1];
        $itemType = isset($this->sectionToTypeMap[$section]) ? $this->sectionToTypeMap[$section] : null;
        $entityN  = "Application\\Entity\\{$itemType}";
        $builder  = new AnnotationBuilder();
        $entity   = new $entityN();
        $form     = $builder->createForm($entity);

        $form->setHydrator($hydrator);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();

        if ($entityN == 'Application\Entity\Type' && isset($data['category'])) {
            $cat = $data['category'];
            if (is_array($cat) && array_key_exists('id', $cat)) {
                $cat = $cat['id'];
            }

            $cat = $em->find('Application\Entity\TypeCategory', $cat);
            if (!$cat) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('error' => 'invalid category'));
            }

            $entity->setCategory($cat);
        }

        if ($entityN == 'Application\Entity\Resource' && isset($data['category'])) {
            $cat = $data['category'];
            if (is_array($cat) && array_key_exists('id', $cat)) {
                $cat = $cat['id'];
            }

            $cat = $em->find('Application\Entity\ResourceCategory', $cat);
            if (!$cat) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('error' => 'invalid category'));
            }

            $entity->setCategory($cat);
        }

		$entity->setModifiedDate(new \DateTime('now'));

        $em->persist($entity);
        $em->flush();

        return new JsonModel($this->extract($entity));
    }

    public function update($id, $data)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $path     = explode('/', trim($this->getRequest()->getUri()->getPath(), '/'));
        $section  = $path[1];
        $itemType = isset($this->sectionToTypeMap[$section]) ? $this->sectionToTypeMap[$section] : null;

        $entity   = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\\Entity\\{$itemType}")
            ->find($id);

        if (!$entity) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);

        $preData = $hy->extract($entity);
        $data    = array_merge($preData, $data);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity  = $form->getData();
        $entityN = get_class($entity);

        if ($entityN == 'Application\Entity\Type' && isset($data['category'])) {
            $cat = $em->find('Application\Entity\TypeCategory', $data['category']);

            if (!$cat) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('error' => 'invalid category'));
            }

            $entity->setCategory($cat);
        }

        if ($entityN == 'Application\Entity\Resource' && isset($data['category'])) {
            $cat = $em->find('Application\Entity\ResourceCategory', $data['category']);
            if (!$cat) {
                $this->getResponse()->setStatusCode(400);
                return new JsonModel(array('error' => 'invalid category'));
            }

            $entity->setCategory($cat);
        }

		$entity->setModifiedDate(new \DateTime('now'));
        $em->persist($entity);
        $em->flush();

        return new JsonModel($this->extract($entity));
    }

    public function delete($id)
    {
        $path     = explode('/', trim($this->getRequest()->getUri()->getPath(), '/'));
        $section  = $path[1];
        $itemType = isset($this->sectionToTypeMap[$section]) ? $this->sectionToTypeMap[$section] : null;

        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\\Entity\\{$itemType}")
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        try {
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->remove($result);
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->flush();
        } catch (\Exception $e) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('stats' => 'error'));
        }

        return new JsonModel(array('stats' => 'ok'));
    }
}
