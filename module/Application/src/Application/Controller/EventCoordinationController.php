<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Entity\EventCoordination;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class EventCoordinationController extends AbstractRestfulController
{
    public function create($data)
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $evId  = $this->params()->fromRoute('event_id', null);
        $event = $em->getRepository('Application\Entity\Event')->find($evId);
        if (!$event instanceof Event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $event->setUpdateTime(new \DateTime('now'));

        foreach ($data['coordinations'] as $row) {
            $resId = $row['id'];
            $desc  = isset($row['description']) ? $row['description'] : null;

            $res = $em->getRepository('Application\Entity\Coordination')->find($resId);
            if ($res) {
                $evres = new EventCoordination();
                $evres->setEvent($event);
                $evres->setCoordination($res);
                $evres->setDescription($desc);
                $em->persist($evres);
            }
        }

        $em->flush();

        return new JsonModel(array('status' => 'ok'));
    }
}
