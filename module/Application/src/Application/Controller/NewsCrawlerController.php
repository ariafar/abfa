<?php
namespace Application\Controller;

use SimpleHtmlDom;
use GuzzleHttp;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Application\Entity\ExternalNews;
use Application\Entity\Source;
use Application\Entity\SourceLink;
use PicoFeed\Reader\Reader;

class NewsCrawlerController extends AbstractActionController
{
    protected $keywords;

    public function crawlAction()
    {
        $this->fillValidKeywords();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        while (true) {
            $news = $em->getRepository('Application\Entity\ExternalNews')
                ->findOneBy(array('crawled' => false, 'crawling' => false));

            if (!$news) {
                var_dump("Not Found");
                break;
            }

            $news->setCrawling(true);
            $em->flush();

            $this->fetchBody($news);

            $news->setCrawled(true);
            $news->setCrawling(false);
            $em->flush();
        }

        sleep(5);
        return "Done";
    }

    protected function fillValidKeywords()
    {
        $rows = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\Keyword')
            ->findAll();

        foreach ($rows as $row) {
            $tag   = trim($row->getTag());
            $alias = @(array) json_decode($row->getAliases());

            foreach (array_merge($alias, array($tag)) as $a) {
                $this->keywords[$a] = $tag;
            }
        }
    }

    protected function fetchBody(ExternalNews $item)
    {
        $source = $item->getSource();
        $client = new GuzzleHttp\Client(
            array(
                'defaults' => array(
                    'headers' => array(
                        'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'
                    )
                )
            )
        );

        try {
            $resp = $client->get($item->getLink());
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            return;
        }

        if ($resp->getStatusCode() != '200') {
            //TODO log errors
            return;
        }

        //$dome = SimpleHtmlDom\str_get_html($resp->getBody());
        $dom = \pQuery::parseStr((string)$resp->getBody());

        $body  = $this->getTagContents($dom, $source->getBodypattern());
        $head  = $this->getTagContent($dom, $source->getHeadlinepattern());
        $title = $this->getTagContent($dom, $source->getTitlepattern());
        $image = $this->getTagAttribute($dom, $source->getPhotopattern(), 'src');
        $lead  = $this->getTagContent($dom, $source->getLeadpattern());
        $lead  = trim(strip_tags($lead));

        $body  = preg_replace("/<script.*?>(.*)?<\/script>/im","$1", $body);
        $item->setBody($body);
        $item->setImage('/image/' . base64_encode($image));
        if (!empty($lead)) {
            $item->setLead($lead);
        }

        $item->setTitle(strip_tags($title));
        $item->setHeadline(strip_tags($head));
        $item->setCrawled(true);

        $this->fetchKeywords($item);
        $item->setValid($item->getTags()->count() > 0);

        $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->flush($item);

        var_dump("Fetch news: " . $item->getLink());
    }

    protected function ngram($text, $size = 2)
    {
        $items  = explode(" ", $text);
        $result = array();
        $count  = count($items) - $size + 1;

        $i = 0;
        while ($i < $count) {
            $item = array();
            for ($j = 0; $j < $size; $j++) {
                $k = $i + $j;
                $item[] = $items[$k];
            }

            $i++;

            $result[] = join(' ', $item);
        }

        return $result;
    }

    protected function fetchKeywords($news)
    {
        $html  = "<p>" . $news->getTitle() . "</p>\n\n";
        $html .= "<p>" . $news->getLead()  . "</p>\n\n";
        $html .= "<p>" . $news->getBody()  . "</p>\n\n";
        $text  = strip_tags($html);
        $text  = str_replace(':', ' ', $text);
        $text  = str_replace('.', ' ', $text);
        $text  = str_replace('«', ' ', $text);
        $text  = str_replace('»', ' ', $text);
        $text  = str_replace('  ', ' ', $text);

        $keywords = array();

        foreach (array(5,4,3,2,1) as $i) {
            $find = true;

            while ($find) {
                $find = false;

                foreach ($this->ngram($text, $i) as $_ngram) {
                    if (isset($this->keywords[$_ngram])) {
                        $keywords[] = $this->keywords[$_ngram];
                        $find = true;

                        $text = str_replace($_ngram, '', $text);
                    }

                    if ($find) {
                        break;
                    }
                }
            }
        }

        foreach ($keywords as $tag) {
            $keyword = $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager')
                ->getRepository('Application\Entity\Keyword')
                ->getByTag($tag);

            if ($keyword) {
                $news->addTag($keyword);
            }
        }
    }

    protected function getTagContents($dom , $pattern)
    {
        $html = "";
        foreach ($dom->query($pattern) as $tag) {
            $html .= $tag->html();
        }
        return $html;

        $tag = $dom->find($pattern, 0);

        if (!$tag) {
            return null;
        }

        return $tag->outertext;
    }

    protected function getTagContent($dom , $pattern)
    {
        if (empty($pattern)) {
            return '';
        }

        return $dom->query($pattern)->html();

        $tag = $dom->find($pattern, 0);

        if (!$tag) {
            return null;
        }

        return $tag->outertext;
    }

    protected function getTagAttribute($dom, $pattern, $attr)
    {
        $tag = $dom->query($pattern);
        return $tag->attr($attr);

        $tag = $html->find($pattern, 0);

        if (!$tag) {
            return null;
        }

        return $tag->$attr;
    }
}
