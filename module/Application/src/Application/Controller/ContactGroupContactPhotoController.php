<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\ContactGroup,
    Application\Entity\Contact,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class ContactGroupContactPhotoController extends AbstractRestfulController
{
    protected function getGroup()
    {
        $groupId = $this->params()->fromRoute('group_id');
        return $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\ContactGroup")
            ->find($groupId);
    }

    protected function getContact()
    {
        $id = $this->params()->fromRoute('contact_id');
        return $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository("Application\Entity\Contact")
            ->find($id);
    }

    public function create($data)
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $contact = $this->getContact();
        $group   = $this->getGroup();

        if (!$group || !$contact) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $file = $req->getFiles('photo');
        if (!$file) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => 'Invalid photo'));
        }

        if ($file['error']) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => 'Error'));
        }

        $validator = new \Zend\Validator\File\IsImage();

        if (!$validator->isValid($file)) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => 'Error'));
        }

        $image = $this->saveImage($file);

        $contact->setImage($image);

        $em->flush();

        return new JsonModel($this->extract($contact));
    }

    public function deleteList()
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $contact = $this->getContact();
        $group   = $this->getGroup();

        if (!$group || !$contact) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        unlink('public/' . $contact->getImage());

        $contact->setImage();
        $em->flush();

        return new JsonModel($this->extract($contact));
    }

    protected function saveImage($file)
    {
        $imageName  = substr(md5(microtime()), 0, 10);
        $imageName .= '-';
        $imageName .= md5(microtime() . $file['name']);
        $imageName .= '.jpg';
        $baseDir    = realpath('public/');
        $directory  = '/files/images/'
            . substr($imageName, 0, 2) . '/' . substr($imageName, 2, 2);

        if (!is_dir($baseDir . DIRECTORY_SEPARATOR . $directory)) {
            mkdir($baseDir . DIRECTORY_SEPARATOR . $directory, 0777, true);
        }

        $path = $baseDir . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($file['tmp_name'], $path);

        return $directory . '/' . $imageName;
    }
}
