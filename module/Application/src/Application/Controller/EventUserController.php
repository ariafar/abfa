<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class EventUserController extends AbstractRestfulController
{
    public function getList()
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $evId  = $this->params()->fromRoute('event_id', null);
        $event = $em->getRepository('Application\Entity\Event')->find($evId);
        if (!$event instanceof Event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $data = $this->extract($event);
        $data['user'] = $this->extract($event->getCreator());

        $data['users'] = array();
        foreach ($event->getUser() as $user) {
            $data['users'][] = $this->extract($user);
        }

        return new JsonModel($data);
    }

    public function create($data)
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $evId  = $this->params()->fromRoute('event_id', null);
        $event = $em->getRepository('Application\Entity\Event')->find($evId);
        if (!$event instanceof Event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $event->setUpdateTime(new \DateTime('now'));

        $data['users'] = isset($data['users']) ? $data['users'] : array();

        $users = array();
        foreach ($data['users'] as $uid) {
            $user = $em->getRepository('Application\Entity\User')->find($uid);

            if ($user) {
                $event->removeUser($user);
                $event->addUser($user);

                $users[] = $user;
            }
        }

        $em->flush();

        $data = $this->extractEvent($event);
        $this->notifyUser($event, $data, $users);

        return new JsonModel(array('status' => 'ok'));
    }
}
