<?php
namespace Application\Controller;

use Application\Entity\Event;
use Application\Entity\EventPhoto;
use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;

class EventPhotoController extends AbstractRestfulController
{
    public function create($data)
    {
        $em  = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy  =  $this->getServiceLocator()->get('Hydrator');
        $req = $this->getRequest();

        $evId  = $this->params()->fromRoute('event_id', null);
        $event = $em->getRepository('Application\Entity\Event')->find($evId);
        if (!$event instanceof Event) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $event->setUpdateTime(new \DateTime('now'));

        $files = $req->getFiles()->toArray();
        foreach ($files as $file) {
            if ($file['error']) {
                return new JsonModel(array('error' => 'Error'));
            }

            $validator = new \Zend\Validator\File\IsImage();

            if (!$validator->isValid($file)) {
                return new JsonModel(array('error' => 'Error'));
            }

            $image = $this->saveImage($file);

            $photo = new EventPhoto();
            $photo->setEvent($event);
            $photo->setImage($image);
            $photo->setUploadDate(new \DateTime('now'));

            $em->persist($photo);
        }

        $em->flush();

        $data = $this->extractEvent($event);
        $this->notifyUsers($event, $data, $this->identity());

        return new JsonModel($this->extract($photo));
    }

    protected function saveImage($file)
    {
        $imageName  = substr(md5(microtime()), 0, 10);
        $imageName .= '-';
        $imageName .= md5(microtime() . $file['name']);
        $imageName .= '.jpg';
        $baseDir    = realpath('public/');
        $directory  = '/files/images/'
            . substr($imageName, 0, 2) . '/' . substr($imageName, 2, 2);

        if (!is_dir($baseDir . DIRECTORY_SEPARATOR . $directory)) {
            mkdir($baseDir . DIRECTORY_SEPARATOR . $directory, 0777, true);
        }

        $path = $baseDir . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($file['tmp_name'], $path);

        return $directory . '/' . $imageName;
    }
}
