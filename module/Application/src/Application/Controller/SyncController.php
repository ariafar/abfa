<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class SyncController extends AbstractActionController
{
    public function syncAction()
    {
        $sm = $this->getServiceLocator();
        $hy = $sm->get('Hydrator');
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $authService = $sm->get('Zend\Authentication\AuthenticationService');
        if (!$authService->hasIdentity()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

		$lastUpdate = $this->params()->fromQuery('lastUpdate', null);
        $causes = array();
        foreach ($em->getRepository('Application\Entity\Cause')->findAllByLastUpdate($lastUpdate) as $r) {
            $causes[] = $hy->extract($r);
        }

        $typeCats = array();
        foreach ($em->getRepository('Application\Entity\TypeCategory')->findAllByLastUpdate($lastUpdate) as $r) {
            $typeCats[] = $hy->extract($r);
        }

        $types = array();
        foreach ($em->getRepository('Application\Entity\Type')->findAllByLastUpdate($lastUpdate) as $r) {
            $d = $hy->extract($r);
            $c = $r->getCategory();
            if ($c) {
                $d['category_id'] = $c->getId();
            }
            $types[] = $d;
        }

        $levels = array();
        foreach ($em->getRepository('Application\Entity\Level')->findAllByLastUpdate($lastUpdate) as $r) {
            $levels[] = $hy->extract($r);
        }

        $regions = array();
        foreach ($em->getRepository('Application\Entity\Region')->findAllByLastUpdate($lastUpdate) as $r) {
            $regions[] = $hy->extract($r);
        }

        $resCats = array();
        foreach ($em->getRepository('Application\Entity\ResourceCategory')->findAllByLastUpdate($lastUpdate) as $r) {
            $resCats[] = $hy->extract($r);
        }

        $resources = array();
        foreach ($em->getRepository('Application\Entity\Resource')->findAllByLastUpdate($lastUpdate) as $r) {
            $d = $hy->extract($r);
            $c = $r->getCategory();
            if ($c) {
                $d['category_id'] = $c->getId();
            }
            $resources[] = $d;
        }

        $coords = array();
        foreach ($em->getRepository('Application\Entity\Coordination')->findAllByLastUpdate($lastUpdate) as $r) {
            $coords[] = $hy->extract($r);
        }

		$updateDate = new \DateTime('now');
        return new JsonModel(array(
			'updateDate' => $updateDate->format("Y-m-d H:i:s"),
            'type_categories' => $typeCats,
            'resource_categories' => $resCats,
            'types'   => $types,
            'levels'  => $levels,
            'regions' => $regions,
            'causes'  => $causes,
            'resources' => $resources,
            'coordinations' => $coords
        ));
    }
}
