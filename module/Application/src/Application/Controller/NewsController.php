<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\News,
    Application\Entity\NewsPhoto,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class NewsController extends AbstractRestfulController
{
    public function getList()
    {
        $lastTime = $this->getRequest()->getQuery('lastUpdateTime', null);
        $limit    = $this->getRequest()->getQuery('limit', 10);
        $before   = $this->getRequest()->getQuery('before', null);
        $after    = $this->getRequest()->getQuery('after', null);

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $qb = $em->createQueryBuilder();

        $qb->select('n')->from('Application\Entity\News', 'n');

        $and = $qb->expr()->andX();

        if (!empty($lastTime)) {
            $and->add($qb->expr()->gt('n.updateDate', ':lastUpdateTime'));
            $qb->setParameter("lastUpdateTime", new \DateTime($lastTime));
        }

        if ($before) {
            $and->add($qb->expr()->lt('n.creationDate', ':before'));
            $qb->setParameter('before', $before);
        }

        if ($after) {
            $and->add($qb->expr()->gt('n.creationDate', ':after'));
            $qb->setParameter('after', $after);
        }

        if ($and->count() > 0) {
            $qb->where($and);
        }

        $qb->orderBy('n.updateDate', 'DESC');
        $qb->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();
        $return = array();
        $update = new \DateTime('now');

        foreach ($result as $row) {
            $news     = $this->extract($row);
            $news['photos'] = array();

            foreach ($this->getImages($row) as $img) {
                $news['photos'][] = $this->extract($img);
            }

            $update   = $row->getUpdateDate();
            $return[] = $news;
        }

        $now = new \DateTime('now');
        return new JsonModel(
            array(
                'items' => $return,
                'updateTime' => $update->format('Y-m-d H:i:s'),
                'currentTime' => $now->format('Y-m-d H:i:s')
            )
        );
    }

    public function get($id)
    {
        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\News')
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $news = $this->extract($result);
        $news['photos'] = array();

        foreach ($this->getImages($result) as $img) {
            $news['photos'][] = $this->extract($img);
        }

        return new JsonModel($news);
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();
        $entity   = new News();
        $form     = $builder->createForm($entity);

        $form->setHydrator($hydrator);
        $form->bind($entity);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();

        $em->persist($entity);

        $files = $this->getRequest()->getFiles()->toArray();
        $files = isset($files['files']) ? $files['files'] : array();
        foreach ($files as $file) {
            if ($file['error']) {
                return new JsonModel(array('error' => 'Error'));
            }

            $validator = new \Zend\Validator\File\IsImage();

            if (!$validator->isValid($file)) {
                return new JsonModel(array('error' => 'Error'));
            }

            $image = $this->saveImage($file);

            $photo = new NewsPhoto();
            $photo->setNews($entity);
            $photo->setImage($image);
            $photo->setUploadDate(new \DateTime('now'));

            $em->persist($photo);
        }

        $entity->setUpdateDate(new \DateTime('now'));
        $entity->setCreationDate(new \DateTime('now'));

        $em->flush();

        $news = $this->extract($entity);
        $news['photos'] = array();

        foreach ($this->getImages($entity) as $img) {
            $news['photos'][] = $this->extract($img);
        }

        return new JsonModel($news);
    }

    public function update($id, $data)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hy = $this->getServiceLocator()->get('Hydrator');

        $entity = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\News')
            ->find($id);

        if (!$entity) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($entity);

        $form->setHydrator($hy);
        $form->bind($entity);

        $preData = $hy->extract($entity);
        $data    = array_merge($preData, $data);
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }

        $entity = $form->getData();
        $entity->setUpdateDate(new \DateTime('now'));

        $files = $this->getRequest()->getFiles()->toArray();
        foreach ($files as $file) {
            if ($file['error']) {
                return new JsonModel(array('error' => 'Error'));
            }

            $validator = new \Zend\Validator\File\IsImage();

            if (!$validator->isValid($file)) {
                return new JsonModel(array('error' => 'Error'));
            }

            $image = $this->saveImage($file);

            $photo = new NewsPhoto();
            $photo->setNews($entity);
            $photo->setImage($image);
            $photo->setUploadDate(new \DateTime('now'));

            $em->persist($photo);
        }

        $em->flush();

        $news = $this->extract($entity);
        $news['photos'] = array();

        foreach ($this->getImages($entity) as $img) {
            $news['photos'][] = $this->extract($img);
        }

        return new JsonModel($news);
    }

    public function delete($id)
    {
        $result = $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\News')
            ->find($id);

        if (!$result) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $result->setDeleted(true);
        $result->setUpdateDate(new \DateTime('now'));

        try {
            $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->flush();
        } catch (\Exception $e) {
            return new JsonModel(array('stats' => 'error'));
        }

        $news = $this->extract($result);
        $news['photos'] = array();

        foreach ($this->getImages($result) as $img) {
            $news['photos'][] = $this->extract($img);
        }

        return new JsonModel($news);
    }

    protected function saveImage($file)
    {
        $imageName  = substr(md5(microtime()), 0, 10);
        $imageName .= '-';
        $imageName .= md5(microtime() . $file['name']);
        $imageName .= '.jpg';
        $baseDir    = realpath('public/');
        $directory  = '/files/news/'
            . substr($imageName, 0, 2) . '/' . substr($imageName, 2, 2);

        if (!is_dir($baseDir . DIRECTORY_SEPARATOR . $directory)) {
            mkdir($baseDir . DIRECTORY_SEPARATOR . $directory, 0777, true);
        }

        $path = $baseDir . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($file['tmp_name'], $path);

        return $directory . '/' . $imageName;
    }

    protected function getImages($news)
    {
        return $this->getServiceLocator()
            ->get('Doctrine\ORM\EntityManager')
            ->getRepository('Application\Entity\NewsPhoto')
            ->findBy(array('news' => $news));
    }
}
