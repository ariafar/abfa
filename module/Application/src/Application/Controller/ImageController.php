<?php
namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    Application\Entity\News,
    Application\Entity\NewsPhoto,
    Zend\Form\Annotation\AnnotationBuilder,
    Zend\View\Model\JsonModel,
    Doctrine\Common\Annotations\AnnotationReader;

class ImageController extends AbstractRestfulController
{
    public function getAction()
    {
        $code = $this->params()->fromRoute('hash', '');
        $url  = base64_decode($code);

        return $this->redirect()->toUrl($url);
    }
}
